// Include the library
#include <Arduino.h>
#include <SensorSoilMoisture.h>

using namespace std;

// Initialize global variables
const int soilMoistureSensor = A0; //A0

namespace SensorSoilMoisture
{
    // Initialize sensor value
    int soilMoistureValue = 0;
    int getSoilMoistureValue() {
        soilMoistureValue = analogRead(soilMoistureSensor);
        return soilMoistureValue = map(soilMoistureValue, 0, 1024, 0, 100);
    }
} 


