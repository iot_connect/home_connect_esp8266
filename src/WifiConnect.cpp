#include <ESP8266WiFi.h>

using namespace std;

namespace Connection {
    /*
    * Connect your controller to WiFi
    */
    void wifiConnect() {
    // Const param
    const char *ssid = "XXX";
    const char *password = "XXX";
    Serial.println();
    Serial.println();
    Serial.print("Connecting to WiFi");
    Serial.println("...");
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    int retries = 0;
    while ((WiFi.status() != WL_CONNECTED) && (retries < 15)) {
    retries++;
    delay(500);
    Serial.print(".");
    }
    if (retries > 14) {
        Serial.println(F("WiFi connection FAILED"));
    }
    if (WiFi.status() == WL_CONNECTED) {
        Serial.println(F("WiFi connected!"));
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
    }
        Serial.println(F("Setup ready"));
    }
}
    