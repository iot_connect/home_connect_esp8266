// Include the library
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Mqtt.h>

using namespace std;

const char* mqttServer = "test.mosquitto.org"; // Adress mqtt server

WiFiClient espClient;
PubSubClient client(espClient);

namespace Connection
{
    void mqttCallback(char* topic, byte* payload, unsigned int length) {
        Serial.print("Message arrived [");
        Serial.print(topic);
        Serial.print("] ");
        for (int i = 0; i < length; i++) {
            Serial.print((char)payload[i]);
        }  Serial.println();
    }

    void mqttPublish(char* topic, char* data) {
        // Here precise topic name and data want to send
        client.publish(topic, data);
    }

    void mqttSubscribe(char* topic) {
        // Here precise topic name who want subscribe
        client.subscribe(topic);
    }

    void mqttReconnect(char* topic) {
    // Loop until we're reconnected
        while (!client.connected()) {
            Serial.println("Attempting MQTT connection...");
            // Precise client id name and add a random string
            String clientId = "ESP8266-YNOV-";
            clientId += String(random(0xffff), HEX);
            // Attempt to connect
            Serial.print("Client : " + clientId);
            if (client.connect(clientId.c_str())) {
                Serial.println(" [connected]");
                mqttSubscribe(topic);
            } else {
                Serial.print("failed, rc=");
                Serial.print(client.state());
                Serial.println(" try again in 5 seconds");
                // Wait 5 seconds before retrying
                delay(5000);
            }
        }
    }

    void mqttConnect(char* topic){
        client.setServer(mqttServer, 1883);
        client.setCallback(mqttCallback);
        mqttSubscribe(topic);
    }

    void mqtt(char* topic, char* data) {
    if (!client.connected()) {
        mqttReconnect(topic);
    }
    client.loop();

    mqttPublish(topic, data);
    }
} 


