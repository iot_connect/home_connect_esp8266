// Include the library 
#include <Arduino.h>

using namespace std;

// Initialize global variables
const int lightSensor = A0; //A0

namespace SensorLight {
    // Initialise sensor value
    int lightSensorValue = 0;
    /* Setup mode for sensor light */
    void setup() {
        pinMode(lightSensor, INPUT);
    }

    int getLightSensorValue(){
        // Get Light Sensor value
        return lightSensorValue = analogRead(lightSensor);
    } 
}