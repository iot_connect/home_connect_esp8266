#include <ESP8266WiFi.h> 
#include "WifiConnect.h"
#include "Mqtt.h"
#include "RgbLight.h"
#include "SensorDht11.h"
#include "SensorLight.h"
#include "SensorSoilMoisture.h"

// Initialize variable
char* topicSensorDht11Temperature = "YNOV/DHT11/bc:dd:c2:10:7b:14/TEMP";
char* topicSensorDht11Humidity = "YNOV/DHT11/bc:dd:c2:10:7b:14/HUM";
char* topicSensorSoilMoisture = "YNOV/SOILMOISTURE/bc:dd:c2:10:7b:14/HUM";
char* topicSensorLight = "YNOV/SENSORLIGHT/bc:dd:c2:10:7b:14/";

void setup()
{
  // Initialize default speed transmition to monitor
  Serial.begin(9600);

  Connection::wifiConnect(); 
  Connection::mqttConnect(topicSensorDht11Temperature);
  // Subscribe to mqtt topic 
  // Connection::mqttSubscribe(topic); // TODO : Possible de le faire dans le setup ?
  // RgbLight::setup();
  // SensorLight::setup();
  SensorDht11::setup();
}

void loop() 
{
  //RgbLight::lightTest();
  //RgbLight::lightDetection();

  Serial.println("Temperature : " + String(SensorDht11::getTemperatureSensorValue()) + " °C"); // We cast in string to display and concatenate string and int

  Serial.println("Humidity : " + String(SensorDht11::getHumiditySensorValue()) + " %");

  //Serial.println("Heat index : " + String(SensorDht11::getComputeHeatIndex()) + " °C");

  //Serial.println("Soil moisture : " + String(SensorSoilMoisture::getSoilMoistureValue()) + " %");

  char bufferTemperature[10], bufferHumidity[10], bufferLight[10], bufferSoilMoisture[10];
  float temperature = SensorDht11::getTemperatureSensorValue();
  float humidity = SensorDht11::getHumiditySensorValue();
  //int soilMoisture = SensorSoilMoisture::getSoilMoistureValue();

  sprintf(bufferTemperature, "%.2f", temperature);
  sprintf(bufferHumidity, "%.2f", humidity);
  //sprintf(bufferSoilMoisture, "%d", soilMoisture);

  Connection::mqtt(topicSensorDht11Temperature, bufferTemperature);
  Connection::mqtt(topicSensorDht11Humidity, bufferHumidity);
  //Connection::mqtt(topic, bufferSoilMoisture);
  Connection::mqttSubscribe(topicSensorDht11Temperature);
  Connection::mqttSubscribe(topicSensorDht11Humidity);

  delay(10000);
}