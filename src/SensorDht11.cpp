// Include the library 
#include <Adafruit_Sensor.h>
#include "DHT.h"

using namespace std;

#define DHTTYPE DHT11 

// DHT Sensor
uint8_t dhtSensor = 02; // D4

// Initialize DHT sensor.
DHT dht(dhtSensor, DHTTYPE);

namespace SensorDht11 {
    void setup() {
        pinMode(dhtSensor, INPUT);
        dht.begin();    
    }

    float getTemperatureSensorValue() {
        // Get the temperature value
        // To have fahrenheit value, added true in parameters of method "readTemperature" 
        float temperature = dht.readTemperature();
        return temperature;  
    }

    float getHumiditySensorValue() {
        // Get the humidity value
        float humidity = dht.readHumidity();
        return humidity;
    }

    float getComputeHeatIndex() {
        // To calculate the heat index
        float temperature = getTemperatureSensorValue();
        float humidity = getHumiditySensorValue(); 
        float headIndex = dht.computeHeatIndex(temperature, humidity, false);
        return headIndex;
    }
} 