// Include the library 
#include <Arduino.h>
#include "RgbLight.h"
#include "SensorLight.h"

using namespace std;

// Initialise global variables
const int redLED = 13; //D7
const int greenLED = 12; //D6
const int blueLED = 14;  //D5

namespace RgbLight {
    /* Setup mode for rgb light */
    void setup() {
        pinMode(redLED, OUTPUT);
        pinMode(greenLED, OUTPUT);
        pinMode(blueLED, OUTPUT);
    }
    /* Rgb led rotation for test */
    void lightTest() {
        digitalWrite(redLED, HIGH);
        delay(1000);
        digitalWrite(redLED, LOW);
        delay(1000);
        digitalWrite(greenLED, HIGH);
        delay(1000);
        digitalWrite(greenLED, LOW);
        delay(1000);
        digitalWrite(blueLED, HIGH);
        delay(1000);
        digitalWrite(blueLED, LOW);
        delay(1000);
    }
    void lightDetection() {
        // Get the sensor value
        int lightSensorValue = SensorLight::getLightSensorValue();
        // Print value in monitor
        Serial.println(lightSensorValue);
        delay(100);

        // Dark mode
        if(lightSensorValue <= 200) {
        digitalWrite(greenLED, LOW);
        digitalWrite(blueLED, LOW);
        digitalWrite(redLED, HIGH);
        }
        // Full light mode
        else if(lightSensorValue >= 700) {
            digitalWrite(redLED, LOW);
            digitalWrite(blueLED, LOW);
            digitalWrite(greenLED, HIGH);
        }
        // Ambient mode
        else {
            digitalWrite(redLED, LOW);
            digitalWrite(greenLED, LOW);
            digitalWrite(blueLED, HIGH);
        }
    }
}


