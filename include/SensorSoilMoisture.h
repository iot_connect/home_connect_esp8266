// If exist
#ifndef SensorSoilMoisture_H
#define SensorSoilMoisture_H

namespace SensorSoilMoisture
{
    int getSoilMoistureValue();
}

#endif // SensorSoilMoisture_H