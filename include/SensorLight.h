// If exist
#ifndef SensorLight_H
#define SensorLight_H

namespace SensorLight
{
    void setup();
    int getLightSensorValue();
}

#endif // SensorLight_H