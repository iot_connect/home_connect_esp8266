// If exist
#ifndef Mqtt_H
#define Mqtt_H

namespace Connection
{
    void mqttCallback(char* topic, byte* payload, unsigned int length);
    void mqttReconnect(char* topic);
    void mqttConnect(char* topic); // Inside main setup
    void mqtt(char* topic, char* data); // Inside main loop
    void mqttPublish(char* topic, char* data);
    void mqttSubscribe(char* topic);
}

#endif // MqttConnection_H