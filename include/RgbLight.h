// If exist
#ifndef RgbLight_H
#define RgbLight_H

//Path with RgbLight()
namespace RgbLight
{
    void setup();
    void lightTest();
    void lightDetection();
}

#endif // RgbLight_H

