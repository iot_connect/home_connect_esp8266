// If exist
#ifndef SensorDht11_H
#define SensorDht11_H

namespace SensorDht11
{
    void setup();
    float getTemperatureSensorValue();
    float getHumiditySensorValue();
    float getComputeHeatIndex();
}

#endif // SensorDht11_H